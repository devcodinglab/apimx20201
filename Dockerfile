#Image base
FROM node:latest

#Directorio de la aplicación
WORKDIR /app

#Copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto expuesto en el contenedor
EXPOSE 3000

#Comando de ejecución
CMD ["npm", "start"]
